class Microwave

  def initialize(number)
    # 3 @minutes , @seconds sont des variables de classe.
    @minutes, @seconds = parse(number) || split(number)
  end

  def timer
    "#{@minutes.to_s.rjust(2,'0')}:#{@seconds.to_s.rjust(2,'0')}"
  end

  private
  def split(number)
    number.to_s.rjust(4,'0').scan(/../)
  end

  def parse(number)
    return nil if number >= 100
    minutes = number / 60
    seconds = number % 60
    [minutes, seconds]
  end
end
