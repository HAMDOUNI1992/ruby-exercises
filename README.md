# Ruby Exercises

## 1/ HELLO WORLD !

### Instructions
The classical introductory exercise. Just say "Hello, World!".

"Hello, World!" is the traditional first program for beginning programming in a new language or environment.

The objectives are simple:

Write a function that returns the string "Hello, World!".
Run the test suite and make sure that it succeeds.
Submit your solution and check it at the website.
If everything goes well, you will be ready to fetch your first real exercise.

```ruby
class HelloWorld
  def self.hello
    "Goodbye, Mars!"
  end
end
```

```ruby
=begin
Write your code for the 'HelloWorld' exercise in this file `hello-world.rb`.
=end
```

## 2/ Microwaves

### Instructions
Convert the buttons entered on the microwave panel to their timer equivalent.

Microwave timers are smart enough to know that when you press 90 for seconds, you mean '01:30', which is 90 seconds. We want to have a "smart display" that will convert this to the lowest form of minutes and seconds, rather than leaving it as 90 seconds.

Build a class that accepts buttons entered and converts them to the proper display panel time.

```ruby
=begin
Write your code for the 'Microwave' exercise in this file `microwave.rb`.
=end
```

## 3/ Meetup

### Instructions
Calculate the date of meetups.

Typically meetups happen on the same day of the week. In this exercise, you will take a description of a meetup date, and return the actual meetup date.

Examples of general descriptions are:

The first Monday of January 2017
The third Tuesday of January 2017
The wednesteenth of January 2017
The last Thursday of January 2017
The descriptors you are expected to parse are: first, second, third, fourth, fifth, last, monteenth, tuesteenth, wednesteenth, thursteenth, friteenth, saturteenth, sunteenth

Note that "monteenth", "tuesteenth", etc are all made up words. There was a meetup whose members realized that there are exactly 7 numbered days in a month that end in '-teenth'. Therefore, one is guaranteed that each day of the week (Monday, Tuesday, ...) will have exactly one date that is named with '-teenth' in every month.

Given examples of meetup dates, each containing a month, day, year, and descriptor calculate the date of the actual meetup. For example, if given "The first Monday of January 2017", the correct meetup date is 2017/1/2.

```ruby
=begin
Write your code for the 'Meetup' exercise in this file. `meetup.rb`.
=end
```

## 4/ Lasagna

### Instructions
In this exercise you're going to write some code to help you cook a brilliant lasagna from your favorite cooking book.

You have four tasks, all related to the time spent cooking the lasagna.

#### 1. Define the expected oven time in minutes

Define the Lasagna::EXPECTED_MINUTES_IN_OVEN constant that returns how many minutes the lasagna should be in the oven. According to the cooking book, the expected oven time in minutes is 40:

```ruby
Lasagna::EXPECTED_MINUTES_IN_OVEN
# => 40
```

#### 2. Calculate the remaining oven time in minutes

Define the Lasagna#remaining_minutes_in_oven method that takes the actual minutes the lasagna has been in the oven as a parameter and returns how many minutes the lasagna still has to remain in the oven, based on the expected oven time in minutes from the previous task.

```ruby
lasagna = Lasagna.new
lasagna.remaining_minutes_in_oven(30)
# => 10
```

#### 3. Calculate the preparation time in minutes

Define the Lasagna#preparation_time_in_minutes method that takes the number of layers you added to the lasagna as a parameter and returns how many minutes you spent preparing the lasagna, assuming each layer takes you 2 minutes to prepare.

```ruby
lasagna = Lasagna.new
lasagna.preparation_time_in_minutes(2)
# => 4
```

#### 4. Calculate the total working time in minutes

Define the Lasagna#total_time_in_minutes method that takes two named parameters: the number_of_layers parameter is the number of layers you added to the lasagna, and the actual_minutes_in_oven parameter is the number of minutes the lasagna has been in the oven. The function should return how many minutes in total you've worked on cooking the lasagna, which is the sum of the preparation time in minutes, and the time in minutes the lasagna has spent in the oven at the moment.

```ruby
lasagna = Lasagna.new
lasagna.total_time_in_minutes(number_of_layers: 3, actual_minutes_in_oven: 20)
# => 26
```

```ruby
class Lasagna
  def remaining_minutes_in_oven(actual_minutes_in_oven)
    raise 'Please implement the Lasagna#remaining_minutes_in_oven method'
  end

  def preparation_time_in_minutes(layers)
    raise 'Please implement the Lasagna#preparation_time_in_minutes method'
  end

  def total_time_in_minutes(number_of_layers:, actual_minutes_in_oven:)
    raise 'Please implement the Lasagna#total_time_in_minutes method'
  end
end
```

```ruby
=begin
Write your code for the 'Lasagna' exercise in this file `lasagna.rb`.
=end
```

## 5/ Simple Calculator

### Instructions
In this exercise you will be building error handling for a simple calculator.

The goal is to have a working calculator that returns a string with the following pattern: `16 + 51 = 67`, when provided with arguments `16`, `51` and `+`.

```ruby
SimpleCalculator.calculate(16, 51, "+")
# => "16 + 51 = 67"
SimpleCalculator.calculate(32, 6, "*")
# => "32 * 6 = 192"
SimpleCalculator.calculate(512, 4, "/")
# => "512 / 4 = 128"
```

#### 1. Handle the code that may raise errors within the method `calculate`

The main method for implementation in this task will be the class method `SimpleCalculator.calculate()` method.
It takes three arguments.
The first two arguments are numbers on which an operation is going to be conducted.
The third argument is of type string and for this exercise it is necessary to implement the following operations:

- addition using the `+` string
- multiplication using the `*` string
- division using the `/` string

#### 2. Handle illegal operations

Update the `SimpleCalculator.calculate()` method to raise an `UnsupportedOperation` exception for unknown operation symbols.

```ruby
SimpleCalculator.calculate(1, 2, '-')
# => Raises an UnsupportedOperation
```

#### 3. Handle invalid arguments

Update the `SimpleCalculator.calculate()` method to raise an `ArgumentError` exception for invalid argument types.

```ruby
SimpleCalculator.calculate(1, '2', '*')
# => Raises an ArgumentError
```

#### 4. Handle DivideByZero exceptions

Update the `SimpleCalculator.calculate()` to handle `ZeroDivisionError` exceptions.
The handling code should return the string with the content `Division by zero is not allowed.`.
Any other exception should not be handled by the `SimpleCalculator.calculate()` method.

```ruby
SimpleCalculator.calculate(512, 0, "/")
# => returns "Division by zero is not allowed."
```

## 6/ Log line parser

### Instructions

In this exercise you'll be processing log-lines.

Each log line is a string formatted as follows: `"[<LEVEL>]: <MESSAGE>"`.

There are three different log levels:

- `INFO`
- `WARNING`
- `ERROR`

You have three tasks, each of which will take a log line and ask you to do something with it.
There are lots of ways to solve these tasks - choose your favourite methods from the examples above and see what you can come up with.

#### 1. Get message from a log line

Implement the `LogLineParser#message` method to return a log line's message:

```ruby
LogLineParser.new('[ERROR]: Invalid operation').message
# => "Invalid operation"
```

Any leading or trailing white space should be removed:

```ruby
LogLineParser.new("[WARNING]:  Disk almost full\r\n").message
# => "Disk almost full"
```

#### 2. Get log level from a log line

Implement the `LogLineParser#log_level` method to return a log line's log level, which should be returned in lowercase:

```ruby
LogLineParser.new('[ERROR]: Invalid operation').log_level
# => "error"
```

#### 3. Reformat a log line

Implement the `LogLineParser#reformat` method that reformats the log line, putting the message first and the log level after it in parentheses:

```ruby
LogLineParser.new('[INFO]: Operation completed').reformat
# => "Operation completed (info)"
```

## 7/ 

### Instructions

```ruby
```

## 8/ 

### Instructions

```ruby
```

## 9/ 

### Instructions

```ruby
```

## 10/ 

### Instructions

```ruby
```
