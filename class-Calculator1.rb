class SimpleCalculator
  def self.calculate(nbr1, nbr2, op)
    if (nbr1.class.to_s != 'Integer' ||  nbr2.class.to_s != 'Integer')
      raise RuntimeError.new("Raises an ArgumentError")
    end
    if (op == '+')
      total = nbr1 + nbr2
      return nbr1.to_s + " " + op + " " + nbr2.to_s + " = " + total.to_s
    end
    if (op == '*')
      total = nbr1 * nbr2
      return nbr1.to_s + " " + op + " " + nbr2.to_s + " = " + total.to_s
    end
    if (op == '/')
      if  (nbr1 == 0 || nbr2 == 0)
        return 'Division by zero is not allowed.'
      end
      total = nbr1 / nbr2
      return nbr1.to_s + " " + op + " " + nbr2.to_s + " = " + total.to_s
    end
    raise RuntimeError.new("Raises an UnsupportedOperatione")
  end
end
begin
simpleCalculator1 = SimpleCalculator.new
puts simpleCalculator1.calculate(20, 51, "+")
simpleCalculator2 = SimpleCalculator.new
puts simpleCalculator2.calculate(16, 51, "*")
simpleCalculator3 = SimpleCalculator.new
puts simpleCalculator3.calculate(16, 51, "/")
puts simpleCalculator3.calculate(0, 51, "/")
puts simpleCalculator3.calculate(20, 0, "/")
puts simpleCalculator3.calculate(0, 0, "/")
puts simpleCalculator3.calculate('0', 0, "/")
puts simpleCalculator3.calculate(0, '0', "/")
puts simpleCalculator3.calculate('0', '0', "/")
rescue => e
  puts eclass SimpleCalculator
  def self.calculate(nbr1, nbr2, op)
    if (nbr1.class.to_s != 'Integer' ||  nbr2.class.to_s != 'Integer')
      raise RuntimeError.new("Raises an ArgumentError")
    end
    if (op == '+')
      total = nbr1 + nbr2
      return nbr1.to_s + " " + op + " " + nbr2.to_s + " = " + total.to_s
    end
    if (op == '*')
      total = nbr1 * nbr2
      return nbr1.to_s + " " + op + " " + nbr2.to_s + " = " + total.to_s
    end
    if (op == '/')
      if  (nbr1 == 0 || nbr2 == 0)
        return 'Division by zero is not allowed.'
      end
      total = nbr1 / nbr2
      return nbr1.to_s + " " + op + " " + nbr2.to_s + " = " + total.to_s
    end
    raise RuntimeError.new("Raises an UnsupportedOperatione")
  end
end
begin
simpleCalculator1 = SimpleCalculator.new
puts simpleCalculator1.calculate(20, 51, "+")
simpleCalculator2 = SimpleCalculator.new
puts simpleCalculator2.calculate(16, 51, "*")
simpleCalculator3 = SimpleCalculator.new
puts simpleCalculator3.calculate(16, 51, "/")
puts simpleCalculator3.calculate(0, 51, "/")
puts simpleCalculator3.calculate(20, 0, "/")
puts simpleCalculator3.calculate(0, 0, "/")
puts simpleCalculator3.calculate('0', 0, "/")
puts simpleCalculator3.calculate(0, '0', "/")
puts simpleCalculator3.calculate('0', '0', "/")
rescue => e
  puts e

end 